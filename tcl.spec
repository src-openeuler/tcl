%define MAJOR 8.6

Name:          tcl
Version:       8.6.16
Release:       1
Epoch:         1
Summary:       The Tool Command Language implementation
License:       TCL AND GPL-3.0-or-later WITH bison-exception-2.2 AND BSD-3-Clause
URL:           https://sourceforge.net/projects/tcl/
Source0:       https://downloads.sourceforge.net/sourceforge/tcl/tcl-core%{version}-src.tar.gz
Source1:       macros.tcl

BuildRequires: autoconf zlib-devel systemtap-sdt-devel gcc
Provides:      tcl(abi) = %{MAJOR}
Obsoletes:     tcl-tcldict <= %{version}
Provides:      tcl-tcldict = %{version}

Patch0:     tcl-8.6.15-autopath.patch
Patch1:     tcl-8.6.15-conf.patch
Patch2:     tcl-8.6.10-hidden.patch
Patch3:     tcl-8.6.10-tcltests-path-fix.patch
Patch4:     stay-out-of-internals-when-nice-interfaces-are-avail.patch
Patch5:     oops.patch
Patch6:     File-not-found-should-be-ignored-silently.patch
Patch7:     tcl-8.6.15-fix-install-mode.patch

%description
Tcl(Tool Command Language) provides a powerful platform for creating integration applications
that tie together diverse applications, protocols, devices, and frameworks. When paired with
the Tk toolkit, Tcl provides the fastest and most powerful way to create GUI applications that
run on linux, Unix, and Mac OS X. Tcl can also be used for a variety of web-related tasks and
for creating powerful command languages for applications.

%package help
Summary: help document for tcl
Requires: %{name} = %{epoch}:%{version}-%{release}
BuildArch: noarch
Provides: %{name}-doc < %{version}-%{release}
Obsoletes: %{name}-doc = %{version}-%{release}

%description help
Help document for tcl.

%package devel
Summary: The development files for tcl
Requires: %{name} = %{epoch}:%{version}-%{release}

%description devel
The development files for tcl.

%prep
%autosetup -n %{name}%{version} -p1

%build
pushd unix
autoconf
%configure --enable-threads --enable-symbols --enable-shared --enable-dtrace
%ifnarch loongarch64
%make_build CFLAGS="%{optflags}" TCL_LIBRARY=%{_datadir}/%{name}%{MAJOR}
%else
%make_build CFLAGS="-Wl,--whole-archive -lpthread -Wl,--no-whole-archive" TCL_LIBRARY=%{_datadir}/%{name}%{MAJOR}
%endif
popd

%install
pushd unix
make INSTALL_ROOT=$RPM_BUILD_ROOT TCL_LIBRARY=%{_datadir}/%{name}%{MAJOR} install
popd

ln -s tclsh%{MAJOR} %{buildroot}%{_bindir}/tclsh
ln -s lib%{name}%{MAJOR}.so $RPM_BUILD_ROOT%{_libdir}/lib%{name}.so

mkdir -p $RPM_BUILD_ROOT%{_libdir}/%{name}%{MAJOR}
ln -s %{_libdir}/%{name}Config.sh $RPM_BUILD_ROOT%{_libdir}/%{name}%{MAJOR}/%{name}Config.sh

mv license.terms COPYING
mkdir -p $RPM_BUILD_ROOT%{_includedir}/%{name}-private/generic
mkdir -p $RPM_BUILD_ROOT%{_includedir}/%{name}-private/unix

find generic unix -name "*.h" -exec cp -p '{}' %{buildroot}/%{_includedir}/%{name}-private/'{}' ';'
(
cd %{buildroot}/%{_includedir}
for header in *.h ; do
    if [ -f %{buildroot}/%{_includedir}/%{name}-private/generic/$header ]; then
        ln -sf ../../$header %{buildroot}/%{_includedir}/%{name}-private/generic
    fi
done
)
sed -i -e "s|$PWD/unix|%{_libdir}|; s|$PWD|%{_includedir}/%{name}-private|" %{buildroot}/%{_libdir}/%{name}Config.sh
rm -rf %{buildroot}/%{_datadir}/%{name}%{MAJOR}/ldAix

install -D -m644 %{S:1} %{buildroot}%{_rpmmacrodir}/macros.tcl

%check
pushd unix
%make_build test
popd

%files
%license COPYING
%doc README.md changes
%{_bindir}/tclsh*
%dir %{_libdir}/%{name}%{MAJOR}
%{_datadir}/%{name}%{MAJOR}
%{_datadir}/%{name}8
%{_libdir}/lib%{name}%{MAJOR}.so
%exclude %{_datadir}/%{name}%{MAJOR}/tclAppInit.c
%{_rpmmacrodir}/macros.tcl

%files devel
%{_includedir}/*
%{_libdir}/lib%{name}stub%{MAJOR}.a
%{_libdir}/lib%{name}.so
%{_libdir}/%{name}Config.sh
%{_libdir}/%{name}ooConfig.sh
%{_libdir}/%{name}%{MAJOR}/%{name}Config.sh
%{_libdir}/pkgconfig/tcl.pc
%{_datadir}/%{name}%{MAJOR}/tclAppInit.c

%files help
%{_mandir}/man?/*

%changelog
* Wed Dec 25 2024 Funda Wang <fundawang@yeah.net> - 1:8.6.16-1
- update to 8.6.16

* Tue Nov 12 2024 Funda Wang <fundawang@yeah.net> - 1:8.6.15-3
- fix library and excutable install mode, which will confuse new debugedit

* Wed Oct 16 2024 Funda Wang <fundawang@yeah.net> - 1:8.6.15-2
- add macros.tcl for downstream packages

* Tue Sep 17 2024 Funda Wang <fundawang@yeah.net> - 1:8.6.15-1
- update to 8.6.15

* Thu Feb 29 2024 wangqia <wangqia@uniontech.com> - 1:8.6.14-1
- Update to version 8.6.14

* Wed Feb 08 2023 fuanan <fuanan3@h-partners.com> - 1:8.6.13-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:drop the old symbol

* Sun Jan 29 2023 zhangruifang2020 <zhangruifang1@h-partners.com> - 1:8.6.13-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 8.6.13

* Tue Nov 15 2022 huajingyun <huajingyun@loongson.cn> - 1:8.6.12-2
- add build flags for weak symbols on loongarch64

* Thu Apr 21 2022 zoulin <zoulin13@h-partners.com> - 1:8.6.12-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 8.6.12

* Fri May 28 2021 yangzhuangzhuang <yangzhuangzhaung1@huawei.com> - 1:8.6.10-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:The "no acceptable C compiler found" error message is displayed during compilation.Therefore,add buildrequires gcc.

* Tue Oct 27 2020 Guoshuai Sun <sunguoshuai@huawei.com> - 1:8.6.10-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: File not found should be ignored silently

* Wed Jul 29 2020 zhangxingliang <zhangxingliang3@huawei.com> - 1:8.6.10-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 8.6.10

* Tue Mar 17 2020 chengquan<chengquan3@huawei.com> - 1:8.6.8.8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Rollback some patches & add /usr/lib64/tcl*

* Sat Jan 11 2020 zhangguangzhi<zhangguangzhi3@huawei.com> - 1:8.6.8.7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:del patch to be consistent with open source

* Wed Dec 25 2019 chengquan <chengquan3@huawei.com> - 1:8.6.8.6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix bug in update help package

* Mon Nov 4 2019 shenyangyang <shenyangyang4@huawei.com> - 1:8.6.8.5
- Type:NA
- ID:NA
- SUG:NA
- DESC:add use/bin/tclsh8.6

* Fri Nov 1 2019 caomeng <caomeng5@huawei.com> - 1:8.6.8.4
- Type:NA
- ID:NA
- SUG:NA
- DESC:fix tclConfig.sh problem

* Mon Sep 09 2019 Huiming Xie <xiehuiming@huawei.com> - 1:8.6.8.3
- Package init

